<?php
// Файл для работы с базой данных

session_start();

// Подключение к базе данных
require_once('db.php');


class DataSqlWorker{

    function __construct($connection)
    {
        $this->connection = $connection;
        $this->json_data = array ();
    }


    // Читстка данных приходящих от пользователя
    function cleanData($connection, $value_field = "") {
        return trim(stripslashes(strip_tags(htmlspecialchars(mysqli_real_escape_string($connection, $value_field)))));
    }


    // Генератор пароля
    function generatePassword($length = 12){
        $chars = 'abdefhiknrstyzABDEFGHKNQRSTYZ234567890';
        $numChars = strlen($chars);
        $string = '';
        for ($i = 0; $i < $length; $i++) {
            $string .= substr($chars, rand(1, $numChars) - 1, 1);
        }
        return $string;
    }


    // Проверка занятости email
    function checkEmailBusy($email){
        $email = $this->cleanData($this->connection, $email);
        $query_get_email = sprintf("SELECT email FROM users WHERE email='%s'", $email);
        $result_user_data = mysqli_query($this->connection, $query_get_email);
        if ($result_user_data->num_rows > 0){

            // Email занят
            $this->json_data['email_busy']=1;
        }
        else{

            // Email свободен
            $this->json_data['email_busy']=0;
        }
        return json_encode($this->json_data);

    }


    // Проверка введенного email
    function checkEmailValid($email){
        $email = $this->cleanData($this->connection, $email);
        if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $this->json_data['email_valid']=0;
        }
        else{
            $this->json_data['email_valid']=1;
        }
        return json_encode($this->json_data);
    }


    // Проверка даты рождения
    function checkDateOfBirth($date_of_birth){
        $date_of_birth  = explode("/", $date_of_birth);
        if (count($date_of_birth) != 3){
            $this->json_data['error_dob']=1;
            return FALSE;
        }
        else{
            if (checkdate($date_of_birth[0], $date_of_birth[1], $date_of_birth[2])){
                $this->json_data['error_dob']=0;
                return $date_of_birth[2].'-'.$date_of_birth[0].'-'.$date_of_birth[1];
            }
            else{
                $this->json_data['error_dob']=1;
                return FALSE;
            }
        }
    }


    // Проверка и сохранение присланного файла изображения
    function checkAndSaveImg(){
            $upload_dir = __DIR__ . "/uploads/";
            $upload_file = $upload_dir . basename($_FILES['nainpic']['name']);
            $url_photo = "uploads/" . $_FILES['nainpic']['name'];
            if (move_uploaded_file($_FILES['nainpic']['tmp_name'], $upload_file)){
                $this->json_data['error_upload_img']=0;
                $this->json_data['url_photo']=$url_photo;
            }
            else{
                $this->json_data['error_upload_img']=1;
            }
            return json_encode($this->json_data);
    }


    // Регистрация клиента
    function registration($first_name, $last_name, $email, $dob, $gender){
        $first_name = $this->cleanData($this->connection, $first_name);
        $last_name = $this->cleanData($this->connection, $last_name);
        $email = $this->cleanData($this->connection, $email);
        $gender = $this->cleanData($this->connection, $gender);
        $password = $this->generatePassword();
        $dob_date = $this->checkDateOfBirth($dob);

        if (json_decode($this->checkEmailBusy($email))->{'email_busy'} == 1){
            exit(json_encode($this->json_data));
        }

        if (json_decode($this->checkEmailValid($email))->{'email_valid'} == 1){
            exit(json_encode($this->json_data));
        }

        if (json_decode($this->checkAndSaveImg())->{'error_upload_img'} == 1){
            $url_photo = json_decode($this->checkEmailValid($email))->{'url_photo'};
            exit(json_encode($this->json_data));
        }

        if ($this->checkDateOfBirth($dob) == FALSE){
            exit(json_encode($this->json_data));
        }

        $url_photo = $this->json_data['url_photo'];

        // Добавление данных в базу
        $query_add_user = "INSERT INTO users (email, password, first_name, last_name, photo, dob, pol)
            VALUES ('$email', '$password', '$first_name', '$last_name', '$url_photo', '$dob_date', '$gender')";
        if (mysqli_query($this->connection, $query_add_user) == FALSE) {
            $this->json_data['error_db_connect']=1;
            exit(json_encode($this->json_data));
        }
        else{
            $this->json_data['error_db_connect']=0;

            // Отправка пароля на почту клиента
            mail($email, "You Password", $password);

            $_SESSION['email'] = $email;
            $_SESSION['first_name'] = $first_name;
            $_SESSION['last_name'] = $last_name;
            $_SESSION['dob'] = $dob_date;
            $_SESSION['pol'] = $gender;
            $_SESSION['url_photo'] = $url_photo;
            exit(json_encode($this->json_data));
        }

    }


    // Вход клиента
    function enter($email, $password){
        $email = $this->cleanData($this->connection, $email);
        $password = $this->cleanData($this->connection, $password);
        $query_get_user = sprintf("SELECT email, first_name, last_name, photo, dob, pol FROM users WHERE email='%s' AND password='%s'", $email, $password);
        $result_user_data = mysqli_query($this->connection, $query_get_user);

        if ($result_user_data->num_rows > 0){
            $user = mysqli_fetch_row($result_user_data);
            $_SESSION['email'] = $user[0];
            $_SESSION['first_name'] = $user[1];
            $_SESSION['last_name'] = $user[2];
            $_SESSION['url_photo'] = $user[3];
            $_SESSION['dob'] = $user[4];
            $_SESSION['pol'] = $user[5];

            $json_data['error_enter']=0;
            echo json_encode($json_data);
        }
        else{
            $json_data['error_enter']=1;
            echo json_encode($json_data);
        }

    }


}
