<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <title>Form for Test Challenge</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="main.css">
    <link rel="stylesheet" href="http://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <script src="http://code.jquery.com/jquery-1.10.2.js"></script>
    <script src="http://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <script src="actions.js"></script>

    <script type="text/javascript">
        $(function() {
            $('#idindob').datepicker({
                defaultDate: "-12y"
            });
        });
    </script>

</head>
<body>


<div class="frm">
    <!-- IDDILAN Start. START Language block -->
    <div id="iddilan" class="clearfix">
        <select onchange="js_worker.switchLanguage(this.value);">
            <option value="RUS">Русский</option>
            <option value="ENG">English</option>
        </select>
    </div>
    <!-- IDDILAN End. END Language block -->

    <!-- REGISTER Start. START Register block -->
    <div id="register">
        <!-- IDDIRTI Start. START Logo and Title block in register section -->
        <div id="iddirti">
            <span id="registrationFormName">Регистрация</span>
        </div>
        <!-- IDDIRTI End. END Logo and Title block in register section -->

        <!-- Registration form field Start -->
        <form  action="routes.php" method="post" enctype="multipart/form-data">

            <!-- Registration form. FIRST NAME field Start -->
            <div class="cldiinp clearfix">
                <label for="idinnam" class="lbl">Имя</label>
                <input id="idinnam" name="nainnam" type="text" class="inp regForm">
            </div>
            <!-- Registration form. FIRST NAME field End -->

            <!-- Registration form. SECOND NAME field Start -->
            <div class="cldiinp clearfix">
                <label for="idinsur" class="lbl">Фамилия</label>
                <input id="idinsur" name="nainsur" type="text" class="inp regForm">
            </div>
            <!-- Registration form. SECOND NAME field End -->

            <!-- Registration form. E-mail field Start -->
            <div class="cldiinp clearfix">
                <label for="idinema" class="lbl">E-mail</label>
                <input id="idinema" name="nainema" type="email" class="inp regForm">
            </div>
            <!-- Registration form. E-mail field End -->

            <!-- Registration form. PICTURE field Start -->
            <div class="cldiinp clearfix">
                <label for="idinpic" class="lbl">Картинка</label>
                <div class="cldifak">
                    <input class="clinfak regForm" type="text" placeholder="Выбрать файл..." name="filename" id="imgField">
                    <button class="clbufak" id="btnOverview">Обзор</button>
                </div>

                <input id="idinpic" name="nainpic" type="file" accept="image/jpeg,image/png" class="inppic">
            </div>
            <!-- Registration form. PICTURE field End -->

            <!-- Registration form. SEX and DOB field Start -->
            <div class="cldiinp clearfix">
                <label for="idindob" class="lbldob">Дата рождения</label>
                <input id="idindob" name="naindob" type="text" class="inpdob regForm">
                <div class="sex">
                    <div>
                        <input id="idinmal" name="nainsex" type="radio" value="Мужской" checked>
                        <label for="idinmal">Мужской</label>
                    </div>
                    <div>
                        <input id="idinfem" name="nainsex" type="radio" value="Женский">
                        <label for="idinfem">Женский</label>
                    </div>
                </div>
            </div>
            <!-- Registration form. SEX and DOB field End -->

            <!-- Registration form. "I Agree" and "Submit button" field Start -->
            <div class="cldiinp clearfix">
                <input id="idinagr" name="nainagr" type="checkbox" onchange="enableRegButton()">
                <label for="idinagr">Я принимаю правила</label>
                <input type="submit" value="Регистрация" id="btnReg" disabled="disabled" onclick="return js_worker.checkRegister();"/>
            </div>

            <script>

                // Делаем кликабельной, кнопку регистрации, после того как клиент принял правила
                function enableRegButton(){
                    if ($('#idinagr').is(':checked') == true){
                        document.getElementById("btnReg").style.cursor = 'pointer';
                        document.getElementById("btnReg").style.background = '#6b5e48';
                        $('#btnReg').removeAttr('disabled')
                    }
                    else{
                        $('#btnReg').attr('disabled','disabled');
                        document.getElementById("btnReg").style.background = '#efefef';
                        document.getElementById("btnReg").style.cursor = 'no-drop';
                    }
                };
            </script>

            <input type="hidden" name="action" value="registration">
            <!-- Registration form. "I Agree" and "Submit button" field End -->

            <!-- Registration form. FORM'S FOOTER Start -->
            <div class="iddifoo">
                <a href="#" onclick="Toggle('login')" id="enterToSiteLink"> Войдите на сайт </a>
            </div>
            <!-- Registration form. FORM'S FOOTER End -->
        </form>
        <!-- Registration form field End -->
    </div>
    <!-- REGISTER End. END Register block -->

    <!-- LOGIN End. END Login block -->
    <div id="login" class="hide">

        <!-- IDDIRTI Start. START Logo and Title block in login section -->
        <div id="idditit">
            <span id="enterFormName">Вход</span>
        </div>
        <!-- IDDIRTI End. END Logo and Title block in login section -->

        <!-- Login form field Start -->
        <form  action="routes.php" method="post">
            <!-- Login form. E-mail field Start -->
            <div class="cldiinp clearfix">
                <label for="idinlog" class="lbl">E-mail</label>
                <input id="idinlog" name="nainema" type="email" class="inp enterForm">
            </div>
            <!-- Login form. E-mail field End -->

            <!-- Login form. Password field Start -->
            <div class="cldiinp clearfix">
                <label for="idinpas" class="lbl">Пароль</label>
                <input id="idinpas" name="nainpas" type="password" class="inp enterForm">
            </div>
            <!-- Login form. Password field End -->

            <!-- Login form. RememberMe field Start -->
            <div class="cldiinp clearfix">
                <input type="hidden" name="action" value="enter">
                <input type="submit" value="Вход" id="btnEnter" onclick="return js_worker.checkEnter();"/>
            </div>

            <script>
                document.getElementById("btnEnter").style.background = '#6b5e48';
                document.getElementById("btnEnter").style.cursor = 'pointer';
            </script>

            <!-- Login form. RememberMe field End -->

            <!-- Login form. FORM'S FOOTER Start -->
            <div class="iddifoo">
                <div id="iddireg">
                    <a href="#" onclick="Toggle('register')" id="registrationLink">Регистрация</a>
                </div>
            </div>
            <!-- Login form. FORM'S FOOTER End -->
        </form>
        <!-- Login form field End -->
    </div>
    <!-- LOGIN End. END Login block -->

</div>
<script>

    var js_worker = new JsWorker();
    js_worker.initializationFormFields();

    function Toggle(form){
        if (form=='register'){
            document.getElementById('login').className='hide';
            document.getElementById('register').className='';
        } else {
            document.getElementById('login').className='';
            document.getElementById('register').className='hide';
        }
    }
    $(document).ready(function() {
        $('.inppic').on('change', function() {
            realVal = $(this).val();
            lastIndex = realVal.lastIndexOf('\\') + 1;
            if(lastIndex !== -1) {
                realVal = realVal.substr(lastIndex);
                $(this).prev('.cldifak').find('.clinfak').val(realVal);
            }
        });
    });
</script>
</body>
</html>
