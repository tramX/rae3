/**
 * Created by Jeka on 01.11.2015.
 */


// Объект для обработки данных
function JsWorker(){

    // Смена языка
    this.switchLanguage = function (language){
        if (language == 'RUS'){
            // Сообщения об ошибках
            localStorage.setItem("error_empty_field", 'Вы не заполнили поле');
            localStorage.setItem("error_email_field", 'Введен неверный Email');
            localStorage.setItem("error_img_field", 'Выбран недопустимый файл');
            localStorage.setItem("error_email_busy", 'Email занят');

            // Названия полей форм
            localStorage.setItem("first_name_field", 'Имя');
            localStorage.setItem("last_name_field", 'Фамилия');
            localStorage.setItem("email_field", 'Email');
            localStorage.setItem("password_field", 'Пароль');
            localStorage.setItem("img_field", 'Картинка');
            localStorage.setItem("dob_field", 'Дата рождения');
            localStorage.setItem("i_accept_the_rules", 'Я принимаю правила');
            localStorage.setItem("button_registration", 'Регистрация');
            localStorage.setItem("button_enter", 'Вход');
            localStorage.setItem("button_overview", 'Обзор');
            localStorage.setItem("gender_male", 'Мужской');
            localStorage.setItem("gender_female", 'Женский');
            localStorage.setItem("enter_to_site_link", 'Войдите на сайт');
            localStorage.setItem("registration_link", 'Регистрация');
            localStorage.setItem("genderinfo_field", 'Пол');
            localStorage.setItem("enter_form_name", 'Вход');
            localStorage.setItem("registration_form_name", 'Регистрация');
            localStorage.setItem("info_form_name", 'Добро пожаловать');


        }
        else{
            // Сообщения об ошибках
            localStorage.setItem("error_empty_field", 'You did not fill the field');
            localStorage.setItem("error_email_field", 'Incorrect Email');
            localStorage.setItem("error_img_field", 'Selected file is not valid');
            localStorage.setItem("error_email_busy", 'Email busy');

            // Названия полей форм
            localStorage.setItem("first_name_field", 'First Name');
            localStorage.setItem("last_name_field", 'Last Name');
            localStorage.setItem("email_field", 'Email');
            localStorage.setItem("password_field", 'Password');
            localStorage.setItem("img_field", 'Picture');
            localStorage.setItem("dob_field", 'Your date of birth');
            localStorage.setItem("i_accept_the_rules", 'I accept the rules');
            localStorage.setItem("button_registration", 'Register');
            localStorage.setItem("button_enter", 'Enter');
            localStorage.setItem("button_overview", 'оverview');
            localStorage.setItem("gender_male", 'Male');
            localStorage.setItem("gender_female", 'Female');
            localStorage.setItem("enter_to_site_link", 'Sign up');
            localStorage.setItem("registration_link", 'Registration');
            localStorage.setItem("genderinfo_field", 'Gender');
            localStorage.setItem("enter_form_name", 'Enter');
            localStorage.setItem("registration_form_name", 'Registration');
            localStorage.setItem("info_form_name", 'Welcome');
        }

        // Присвоение названий полям форм
        try{
            document.querySelector('label[for="idinnam"]').textContent = localStorage.getItem("first_name_field");
        }
        catch (err){

        }

        try{
            document.querySelector('label[for="idinsur"]').textContent = localStorage.getItem("last_name_field");
        }
        catch (err){

        }

        try{
            document.querySelector('label[for="idinpic"]').textContent = localStorage.getItem("img_field");
        }
        catch (err){

        }

        try{
            document.querySelector('label[for="idindob"]').textContent = localStorage.getItem("dob_field");
        }
        catch (err){

        }

        try{
            document.querySelector('label[for="idinpas"]').textContent = localStorage.getItem("password_field");
        }
        catch (err){

        }

        try{
            document.getElementById("btnReg").value = localStorage.getItem("button_registration");
        }
        catch (err){

        }

        try{
            document.getElementById("btnEnter").value = localStorage.getItem("button_enter");
        }
        catch (err){

        }

        try{

        }
        catch (err){

        }

        try{
            document.getElementById("btnOverview").textContent = localStorage.getItem("button_overview");
        }
        catch (err){

        }

        try{
            document.querySelector('label[for="idinagr"]').textContent = localStorage.getItem("i_accept_the_rules");
        }
        catch (err){

        }

        try{
            document.querySelector('label[for="idinmal"]').textContent = localStorage.getItem("gender_male");
        }
        catch (err){

        }

        try{
            document.querySelector('label[for="idinfem"]').textContent = localStorage.getItem("gender_female");
        }
        catch (err){

        }

        try{
            document.getElementById("enterToSiteLink").text = localStorage.getItem("enter_to_site_link");
        }
        catch (err){

        }

        try{
            document.getElementById("registrationLink").text = localStorage.getItem("registration_link");
        }
        catch (err){

        }

        try{
            document.querySelector('label[for="genderinfo"]').textContent = localStorage.getItem("genderinfo_field");
        }
        catch(err){

        }

        try{
            document.getElementById("registrationFormName").textContent = localStorage.getItem("registration_form_name");
        }
        catch (err){

        }

        try{
            document.getElementById("enterFormName").textContent = localStorage.getItem("enter_form_name");
        }
        catch (err){

        }


    };

    // Отправка данных для регистрации
    function sentDataRegistration() {

        console.log(document.getElementById("imgField"));
        // Данные для отправки на сервер
        var aFormData = new FormData();
        aFormData.append("nainpic", document.getElementById("idinpic").files[0]);
        aFormData.append("nainema", document.getElementById("idinema").value);
        aFormData.append("nainnam", document.getElementById("idinnam").value);
        aFormData.append("nainsur", document.getElementById("idinsur").value);
        aFormData.append("naindob", document.getElementById("idindob").value);
        aFormData.append("action", "registration");

        $("input:radio").each(function (i) {
            var $this = $(this);
            if (this.checked) {
                aFormData.append("nainsex", $this.attr('value'));
            }

        });
        console.log(aFormData);


        $.ajax({
            url: "routes.php",
            contentType: false,
            dataType: "text",
            scriptCharset: "CP1251",
            type: "POST",
            processData: false,
            data: aFormData,
            success: function(data){
                var data_result = JSON.parse(data);
                if (data_result['email_busy'] == 1){
                    document.getElementById("idinema").style.backgroundColor = "red";
                    document.getElementById("idinema").value = "";
                    document.getElementById("idinema").setAttribute("placeholder", localStorage.getItem("error_email_busy"));
                }
                else{
                    if (data_result['error_dob'] == 0 && data_result['email_valid'] == 0 && data_result['error_upload_img'] == 0  && data_result['error_db_connect'] == 0){
                        window.location.replace("index.php");
                    }
                }

            },
            error: function(){
                alert('Error Connect');						}
        });
    };


    // Отправка данных для входа
    function sentDataEnter() {

        console.log(document.getElementById("imgField"));
        // Данные для отправки на сервер
        var aFormData = new FormData();
        aFormData.append("nainema", document.getElementById("idinlog").value);
        aFormData.append("nainpas", document.getElementById("idinpas").value);
        aFormData.append("action", "enter");


        $.ajax({
            url: "routes.php",
            contentType: false,
            dataType: "text",
            scriptCharset: "CP1251",
            type: "POST",
            processData: false,
            data: aFormData,
            success: function(data){
                var data_result = JSON.parse(data);
                if (data_result['error_enter'] == 0){
                    window.location.replace("index.php");
                }

            },
            error: function(){
                alert('Error Connect');						}
        });
    };


    // Заполнение полей формы по умолчанию
    this.initializationFormFields = function (){

        if (localStorage.getItem("language") == null){
            localStorage.setItem("language", 'RUS');
        }

        this.switchLanguage(localStorage.getItem("language"));
    };

    // Проверка заполнения полей форм
    checkEmptyField = function (nameForm){
        var input_fields = document.getElementsByClassName(nameForm);
        var a = document.getElementsByClassName(nameForm);
        var i;
        var errors = 0;
        for (i = 0; i < a.length; i++) {
            a[i].style.backgroundColor = "white";
            if (a[i].value == ""){
                a[i].style.backgroundColor = "red";
                a[i].setAttribute("placeholder", localStorage.getItem("error_empty_field"));
                errors = 1;
            }
        }

        if (errors == 0){
            return true;
        }
        else{
            return false;
        }

    };

    // Проверка Email
    checkEmailField = function (emailAddress){
        var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
        return pattern.test(emailAddress);
    };


    // Проверка выбранного файла изображения
    checkImg = function (){
        var types_img = ["gif", "jpg", "png"];
        if (types_img.indexOf(document.getElementById("imgField").value.split(".").pop()) == -1){
            document.getElementById("imgField").style.backgroundColor = "red";
            document.getElementById("imgField").value = "";
            document.getElementById("imgField").setAttribute("placeholder", localStorage.getItem("error_img_field"));
            return false;
        }
        else {
            return true;
        }
        };

    // Регистрация пользователя
    this.checkRegister = function (){
        if (checkEmptyField('regForm') == true){
            if (checkEmailField(document.getElementById("idinema").value) == false){
                document.getElementById("idinema").style.backgroundColor = "red";
                document.getElementById("idinema").value = "";
                document.getElementById("idinema").setAttribute("placeholder", localStorage.getItem("error_email_field"));
            }
            else{
                if (checkImg() == true){
                    sentDataRegistration();
                }
            }

        }
        return false;
    };

    // Вход пользователя
    this.checkEnter = function (){
        console.log('checkEnter');
        if (checkEmptyField('enterForm') == true){
            if (checkEmailField(document.getElementById("idinlog").value) == false){
                document.getElementById("idinlog").style.backgroundColor = "red";
                document.getElementById("idinlog").value = "";
                document.getElementById("idinlog").setAttribute("placeholder", localStorage.getItem("error_email_field"));
            }
            else{
                sentDataEnter();
            }
        }
        return false;
    };

}