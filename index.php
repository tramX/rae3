<?php
session_start();
error_reporting(E_ALL);
ini_set('display_errors', 1);
if (!isset($_SESSION['email'])){
    header("Location: forms.php");
}
?>

<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <title>Form for Test Challenge</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="main.css">
    <script src="http://code.jquery.com/jquery-1.10.2.js"></script>
    <script src="actions.js"></script>

</head>
<body>


<div class="frm" id="userinfo">
    <!-- IDDILAN Start. START Language block -->
    <div id="iddilan" class="clearfix">
        <select onchange="js_worker.switchLanguage(this.value);">
            <option value="RUS">Русский</option>
            <option value="ENG">English</option>
        </select>
    </div>
    <!-- IDDILAN End. END Language block -->


    <!-- LOGIN End. END Login block -->
    <div id="login">

        <!-- IDDIRTI Start. START Logo and Title block in login section -->
        <div id="idditit">
            <span>Добро пожаловать</span>
        </div>
        <!-- IDDIRTI End. END Logo and Title block in login section -->

        <!-- Login form field Start -->
        <form  action="routes.php" method="post">
            <!-- Login form. E-mail field Start -->
            <div class="cldiinp clearfix">
                <label for="idinlog" class="lbl">E-mail</label>
                <?php echo '<input id="idinlog" name="nainema" type="email" class="inp" value=" '.$_SESSION["email"].'">' ?>
            </div>
            <!-- Login form. E-mail field End -->

            <div class="cldiinp clearfix">
                <label for="idinnam" class="lbl">Имя</label>
                <?php echo '<input id="idinnam" name="nainnam" type="text" class="inp" value=" '.$_SESSION["first_name"].'">' ?>
            </div>

            <div class="cldiinp clearfix">
                <label for="idinsur" class="lbl">Фамилия</label>
                <?php echo '<input id="idinsur" name="nainsur" type="text" class="inp" value=" '.$_SESSION["last_name"].'">' ?>
            </div>

            <div class="cldiinp clearfix">
                <label for="genderinfo" class="lbl">Пол</label>
                <?php echo '<input id="genderinfo" name="nainema" type="text" class="inp" value=" '.$_SESSION["pol"].'">' ?>
            </div>

            <div class="cldiinp clearfix">
                <label for="idindob" class="lbldob">Дата рождения</label>
                <?php echo '<input id="idindob" name="naindob" type="text" class="inp" value=" '.$_SESSION["dob"].'">' ?>
            </div>

            <div class="cldiinp clearfix">
                <?php echo '<img src="'.$_SESSION['url_photo'].'" width="250" height="250">' ?>
            </div>


        </form>
        <!-- Login form field End -->
    </div>
    <!-- LOGIN End. END Login block -->

</div>

<script>
    var js_worker = new JsWorker();
    js_worker.initializationFormFields();
</script>

</body>
</html>

